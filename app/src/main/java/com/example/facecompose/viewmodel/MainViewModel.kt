package com.example.facecompose.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.facebook.AccessToken
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import java.util.Date
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val _homeState =
        MutableStateFlow<HomeScreenState>(HomeScreenState.Loading)

    val homeState = _homeState.asStateFlow()

    private val _textState = MutableStateFlow("")

    init {
        viewModelScope.launch(Dispatchers.IO) {
            val currentUser = Firebase.auth.currentUser

            if (currentUser != null) {
                observePosts(currentUser)
            } else {
                _homeState.emit(HomeScreenState.SignInRequired)
            }
        }
    }

    private suspend fun observePosts(currentUser: FirebaseUser) {
        observeFireStore().map { posts ->
            HomeScreenState.Loaded(
                avatarUrl = getUserAvatar(currentUser),
                posts = posts
            )
        }.collect {
            _homeState.emit(it)
        }

        /*_homeState.emit(
            HomeScreenState.Loaded(
                avatarUrl = getUserAvatar(currentUser),
                posts = listOf(
                    Post(
                        text = "",
                        timeStamp = Date(),
                        author = "",
                        authorAvatar = getUserAvatar(currentUser)
                    )
                )
            )
        )*/
    }

    private fun observeFireStore(): Flow<List<Post>> {
        return callbackFlow {
            val listener = Firebase.firestore.collection("posts")
                .addSnapshotListener { value, error ->
                    when {
                        error != null -> {
                            close(error)
                        }
                        value != null -> {
                            val posts = value.map { snapshot ->
                                Post(
                                    text = snapshot.getString("text").orEmpty(),
                                    timeStamp = snapshot
                                        .getDate("date_posted") ?: Date(),
                                    author = snapshot.getString("author_name").orEmpty(),
                                    authorAvatar = snapshot
                                        .getString("author_avatar_url").orEmpty()
                                )
                                /* ALWAYS keep the last post (most recent) at the top */
                            }.sortedBy { it.timeStamp }

                            trySend(posts)
                        }
                    }
                }

            awaitClose { listener.remove() }
        }
    }

    private fun getUserAvatar(currentUser: FirebaseUser): String {
        val accessToken = AccessToken.getCurrentAccessToken()?.token
        /* how to display the picture from user's profile in Firebase */
        return "${requireNotNull(currentUser.photoUrl)}?access_token=$accessToken&type=large"
    }

    fun onSendClicked(text: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _textState.emit(text)

            val postText = _textState.value
            val currentUser = requireNotNull(Firebase.auth.currentUser) {
                "How did you get here ??"
            }

            Firebase.firestore.collection("posts")
                .add(
                    hashMapOf(
                        "text" to postText,
                        "date_posted" to Date(),
                        "author_name" to currentUser.displayName.orEmpty(),
                        "author_avatar_url" to getUserAvatar(currentUser)
                    )
                )
        }
    }
}

sealed class HomeScreenState {
    object Loading : HomeScreenState()

    object SignInRequired : HomeScreenState()

    data class Loaded(
        val avatarUrl: String,
        val posts: List<Post>
    ) : HomeScreenState()
}

data class Post(
    val text: String,
    val timeStamp: Date,
    val author: String,
    val authorAvatar: String
)