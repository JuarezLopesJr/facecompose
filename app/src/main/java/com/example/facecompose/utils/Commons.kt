package com.example.facecompose.utils

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

@Composable
fun VerticalDivider(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colors.onSurface.copy(alpha = 0.12f),
    thickness: Dp = 1.dp,
) {
    val targetThickness = if (thickness == Dp.Hairline) {
        (1f / LocalDensity.current.density).dp
    } else {
        thickness
    }
    Box(
        modifier
            .fillMaxHeight()
            .width(targetThickness)
            .background(color = color)
    )
}

@Composable
fun SigninButton(
    onSignedIn: () -> Unit,
    onSigninFailed: (Exception) -> Unit
) {
    val scope = rememberCoroutineScope()

    AndroidView(
        factory = { context ->
            LoginButton(context).apply {
                setPermissions("email", "public_profile")
                val callbackManager = CallbackManager.Factory.create()
                registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                    override fun onCancel() {
                        TODO("Not yet implemented")
                    }

                    override fun onError(error: FacebookException) {
                        onSigninFailed(error)
                    }

                    override fun onSuccess(result: LoginResult) {
                        scope.launch {
                            val token = result.accessToken.token
                            val credential = FacebookAuthProvider.getCredential(token)
                            val authResult =
                                Firebase.auth.signInWithCredential(credential).await()

                            if (authResult.user != null) {
                                onSignedIn()
                            } else {
                                onSigninFailed(RuntimeException("Could not sign in with Firebase"))
                            }
                        }
                    }
                })
            }
        }
    )
}

data class FriendStory(
    val avatarUrl: String,
    val bgUrl: String,
    val friendName: String,
)