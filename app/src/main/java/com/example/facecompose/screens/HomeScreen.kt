@file:OptIn(
    ExperimentalLifecycleComposeApi::class, ExperimentalLifecycleComposeApi::class,
    ExperimentalFoundationApi::class, ExperimentalLifecycleComposeApi::class
)

package com.example.facecompose.screens

import android.text.format.DateUtils
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.MoreHoriz
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.facecompose.R
import com.example.facecompose.screens.destinations.AppSignInDestination
import com.example.facecompose.screens.destinations.HomeScreenDestination
import com.example.facecompose.viewmodel.HomeScreenState
import com.example.facecompose.viewmodel.MainViewModel
import com.example.facecompose.viewmodel.Post
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import java.util.Date

@RootNavGraph(start = true)
@Destination
@Composable
fun HomeScreen(navigator: DestinationsNavigator) {
    val viewModel = viewModel<MainViewModel>()

    val state by viewModel.homeState.collectAsStateWithLifecycle()

    when (state) {
        is HomeScreenState.Loaded -> {
            val loaded = state as HomeScreenState.Loaded

            HomeScreenContent(
                posts = loaded.posts,
                avatarUrl = loaded.avatarUrl,
                onSendClicked = {
                    viewModel.onSendClicked(it)
                }
            )
        }
        HomeScreenState.Loading -> LoadingScreen()
        HomeScreenState.SignInRequired -> LaunchedEffect(key1 = Unit) {
            navigator.apply {
                popBackStack(HomeScreenDestination, inclusive = true)
                navigate(AppSignInDestination)
            }
        }
    }
}

@Preview
@Composable
private fun HomeScreenContent(
    posts: List<Post> = listOf(
        Post(
            text = "Gus eh um lindo e AMA Jetpack Compose",
            timeStamp = Date(),
            author = "Gus Lindo",
            authorAvatar = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftabris.com%2Fwp-content%2Fuploads%2F2021%2F06%2Fjetpack-compose-icon_RGB-495x400.png&f=1&nofb=1"
        )
    ),
    avatarUrl: String = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftabris.com%2Fwp-content%2Fuploads%2F2021%2F06%2Fjetpack-compose-icon_RGB-495x400.png&f=1&nofb=1",
    onSendClicked: (String) -> Unit = {}
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
    ) {
        LazyColumn(contentPadding = PaddingValues(bottom = 20.dp)) {
            item {
                AppTopBar()
            }

            stickyHeader {
                AppTabBar()
            }

            item {
                AppStatusBar(avatarUrl = avatarUrl, onClickedDone = onSendClicked)
            }

            item {
                StoriesSection()
            }

            items(posts) { post ->
                Spacer(modifier = Modifier.height(8.dp))
                PostCard(post)
            }
        }
    }
}

@Composable
fun PostCard(post: Post) {
    val today = remember { Date() }

    Surface {
        Column {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                AsyncImage(
                    model = ImageRequest.Builder(LocalContext.current)
                        .data(post.authorAvatar)
                        .placeholder(R.drawable.ic_launcher_foreground)
                        .crossfade(true)
                        .build(),
                    contentDescription = null,
                    modifier = Modifier
                        .size(40.dp)
                        .clip(CircleShape)
                )

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .padding(horizontal = 8.dp)
                ) {
                    Text(
                        text = post.author,
                        style = MaterialTheme.typography.body1.copy(
                            fontWeight = FontWeight.Medium
                        )
                    )
                    Text(text = dateLabel(timestamp = post.timeStamp, today = today))
                }

                IconButton(onClick = { }) {
                    Icon(
                        imageVector = Icons.Rounded.MoreHoriz,
                        contentDescription = stringResource(
                            R.string.more_options_button
                        )
                    )
                }
            }

            Text(
                text = post.text,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            )
        }
    }
}

@Composable
private fun dateLabel(timestamp: Date, today: Date): String {
    /* formatting timestamp to describe more friendly date, eg: just now, 5 min ago, 26 Jun */
    return when {
        today.time - timestamp.time < 2 * DateUtils.MINUTE_IN_MILLIS -> "Just now"

        timestamp.time - today.time < DateUtils.DAY_IN_MILLIS -> {
            DateUtils
                .getRelativeTimeSpanString(
                    timestamp.time,
                    today.time,
                    DateUtils.MINUTE_IN_MILLIS,
                    DateUtils.FORMAT_SHOW_DATE
                ).toString()
        }

        else -> ""
    }
}

@Composable
private fun LoadingScreen() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}