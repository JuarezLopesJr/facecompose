package com.example.facecompose.screens

import androidx.compose.foundation.layout.heightIn
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.Menu
import androidx.compose.material.icons.rounded.Newspaper
import androidx.compose.material.icons.rounded.Notifications
import androidx.compose.material.icons.rounded.Store
import androidx.compose.material.icons.rounded.Tv
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.facecompose.R

@Composable
fun AppTabBar() {
    var tabIndex by remember { mutableStateOf(0) }

    val tabs = listOf(
        TabItem(Icons.Rounded.Home, stringResource(R.string.home)),
        TabItem(Icons.Rounded.Tv, stringResource(R.string.reels)),
        TabItem(Icons.Rounded.Store, stringResource(R.string.marketplace)),
        TabItem(Icons.Rounded.Newspaper, stringResource(R.string.news)),
        TabItem(Icons.Rounded.Notifications, stringResource(R.string.notifications)),
        TabItem(Icons.Rounded.Menu, stringResource(R.string.menu)),
    )

    TabRow(
        selectedTabIndex = tabIndex,
        backgroundColor = Color.Transparent,
        /* when using custom colors, i must pass this parameter to both the icon and the
         underline bar to be at the same color */
        contentColor = MaterialTheme.colors.primary
    ) {
        tabs.forEachIndexed { index, tabItem
            ->
            Tab(
                selected = tabIndex == index,
                onClick = { tabIndex = index },
                modifier = Modifier.heightIn(48.dp)
            ) {
                Icon(
                    imageVector = tabItem.icon,
                    contentDescription = tabItem.contentDescription,
                    tint = if (tabIndex == index) {
                        MaterialTheme.colors.primary
                    } else {
                        MaterialTheme.colors.onSurface.copy(alpha = 0.44f)
                    }
                )
            }
        }
    }
}

private data class TabItem(
    val icon: ImageVector,
    val contentDescription: String,
)