package com.example.facecompose.screens

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChatBubble
import androidx.compose.material.icons.rounded.PhotoAlbum
import androidx.compose.material.icons.rounded.VideoCall
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.facecompose.R
import com.example.facecompose.utils.VerticalDivider

@Composable
fun AppStatusBar(
    avatarUrl: String,
    onClickedDone: (String) -> Unit = {}
) {
    val (text, setText) = remember { mutableStateOf("") }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 8.dp, vertical = 12.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current)
                .data(avatarUrl)
                .crossfade(true)
                .placeholder(R.drawable.ic_launcher_foreground)
                .build(),
            contentDescription = null,
            modifier = Modifier
                .size(40.dp)
                .clip(CircleShape)
        )

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange = {
                setText(it)
            },
            placeholder = {
                Text(
                    text = "What's on your kengo ?!",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.typography.body1.color.copy(alpha = 0.44f)
                )
            },
            colors = TextFieldDefaults.textFieldColors(
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                backgroundColor = Color.Transparent
            ),
            singleLine = true,
            keyboardActions = KeyboardActions(
                onSend = {
                    onClickedDone(text)
                    setText("")
                }
            ),
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Send
            )
        )
    }

    Divider()

    Row {
        StatusAction(
            modifier = Modifier.weight(1f),
            imageVector = Icons.Rounded.VideoCall,
            title = "Live"
        )

        VerticalDivider(modifier = Modifier.height(48.dp))

        StatusAction(
            modifier = Modifier.weight(1f),
            imageVector = Icons.Rounded.PhotoAlbum,
            title = "Photo"
        )

        VerticalDivider(modifier = Modifier.height(48.dp))

        StatusAction(
            modifier = Modifier.weight(1f),
            imageVector = Icons.Rounded.ChatBubble,
            title = "Discuss"
        )
    }
}

@Composable
private fun StatusAction(
    modifier: Modifier = Modifier,
    imageVector: ImageVector,
    title: String
) {
    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {
        TextButton(onClick = {}) {
            Icon(
                imageVector = imageVector,
                contentDescription = null,
                modifier = Modifier.padding(start = 16.dp)
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(text = title)
        }
    }
}