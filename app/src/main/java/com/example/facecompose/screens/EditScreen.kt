package com.example.facecompose.screens

import androidx.activity.compose.BackHandler
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.tooling.preview.Preview
import com.example.facecompose.screens.destinations.HomeScreenDestination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.navigation.EmptyDestinationsNavigator

@Preview
@Composable
fun EditContent(
    hasEdits: Boolean = true,
    navigator: DestinationsNavigator = EmptyDestinationsNavigator
) {
    var showWarning by remember { mutableStateOf(false) }

    BackHandler(enabled = hasEdits) {
        showWarning = true
    }

    if (showWarning) {
        AlertDialog(
            onDismissRequest = { showWarning = false },
            text = { Text(text = "Your edits will be lost") },
            dismissButton = {
                TextButton(onClick = { showWarning = false }) {
                    Text(text = "Cancel")
                }
            },
            confirmButton = {
                TextButton(onClick = { navigator.navigate(HomeScreenDestination) }) {
                    Text(text = "Clear")
                }
            }
        )
    }
}