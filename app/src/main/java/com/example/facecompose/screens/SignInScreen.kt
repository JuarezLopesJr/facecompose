package com.example.facecompose.screens

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Facebook
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.example.facecompose.screens.destinations.HomeScreenDestination
import com.example.facecompose.utils.SigninButton
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@Destination
@Composable
fun AppSignIn(navigator: DestinationsNavigator) {
    val context = LocalContext.current

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.surface)
            .padding(top = 120.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Icon(
                imageVector = Icons.Rounded.Facebook,
                contentDescription = null,
                modifier = Modifier.size(90.dp),
                tint = Color.Blue
            )
            Spacer(modifier = Modifier.height(20.dp))

            SigninButton(
                onSignedIn = {
                    navigator.apply {
                        popBackStack()
                        navigate(HomeScreenDestination)
                    }
                },
                onSigninFailed = {
                    Toast.makeText(context, "It's not working!", Toast.LENGTH_LONG).show()
                }
            )
        }
    }
}